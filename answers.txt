:Author: Stephen Tanner (stanner)
:Exercise: 1 
:Due: Sep 11, 11:59PM   
:Markup: reStructuredText
:Editor: http://rst.ninjs.org/ and vim


Question 1
==========

Part a
------

========== ===== ===== ===== =====
Process    PID   PPID  PGID  State
========== ===== ===== ===== =====
dpipe-4404 10739 10637 10739   S
wc         10741 10739 10739   S
gnetcat    10742 10739 10739   S
========== ===== ===== ===== =====

Part b
------

The Sleep state corresponds to the BLOCKED state in the diagram provided in
the lecture slides. 

Part c
------

=============== =========== ============== ================  
File Descriptor depipe-4404    wc            gnetcat
=============== =========== ============== ================ 
(stdin) 0       /dev/pts/13 pipe:[9436654] pipe:[9436653]
(stdout) 1      /dev/pts/13 pipe:[9436653] pipe:[9436653]
(stderr) 2      /dev/pts/13 /dev/pts/13    /dev/pts/13
    3           N/O         N/O            socket:[9436657]
=============== =========== ============== ================


Question 2
==========

Part a
------

::

    pipe([3, 4])                            = 0
    pipe([5, 6])                            = 0
    clone(child_stack=0, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x1196b30) = 5353
    clone(child_stack=0, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x1196b30) = 5355
    close(4)     = 0
    close(5)                              = 0
    close(3)                                = 0
    close(6)                              = 0
    wait4(-1, NULL, 0, NULL) = 5355
    wait4(-1, NULL, 0, NULL)                = 5353
    exit_group(0)                           = ?

Part b
------

::

    write(2, "Starting: wc\n", 13) = 13
    dup2(4, 1) = 1
    close(4) = 0
    dup2(5, 0) = 0
    close(3) = 0
    close(5) = 0
    close(6) = 0
    execve("/home/ugrads/majors/stanner/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)
    execve("/home/courses/cs3214/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOEN T (No such file or directory)
    execve("/home/courses/cs3214/bin/dpipe.variants/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory) 
    execve("/usr/local/lf9562/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory) 
    execve("/home/ugrads/majors/stanner/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)
    execve("/home/courses/cs3214/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)  
    execve("/home/courses/cs3214/bin/dpipe.variants/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)    
    execve("/usr/lib64/qt-3.3/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)   
    execve("/usr/local/lf9562/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory) 
    execve("/usr/local/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)
    execve("/bin/wc", ["wc"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)  
    execve("/usr/bin/wc", ["wc"], [/* 36 vars */]) = 0  


Part c
------

::

    write(2, "Starting: gnetcat\n", 18) = 18
    dup2(3, 0) = 0
    close(5) = 0
    dup2(6, 1) = 1
    close(3)                                = 0
    close(6)                                = 0
    close(4)                                = 0
    execve("/home/ugrads/majors/stanner/bin/gnetcat", ["gnetcat", "-l", "15999"], [/* 36 vars */]) = -1 ENOENT (No such file or directory)
    execve("/home/courses/cs3214/bin/gnetcat", ["gnetcat", "-l", "15999"], [/* 36 vars */]) = 0


Part d
------

1. Process dpipe-4404 is in wait4 system call. This is because it has created two pipes and forked the child processes and is waiting for them to complete. This corresponds to the blocked state from the slides. 
2. Process wc is in the read system call. This is because wc is in a blocked state waiting for input on stdin.
3. Process gnetcat is in the accept system call. This is because it has opened a socket and is listening on that socket for a connection. This is also blocked. 


