//Author:   stanner

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


//Constants
#define ENDS    2
#define STDIN   0
#define STDOUT  1
#define STDERR  2
#define ERR     -1
#define OK      0
//I didn't want to have to use this but it seems that
//with out it the wrong FD is passed
#define FD      5 

int main(int argc, char *argv[]) {    
    //These two vars hold the file descriptors which are the pipe ends for each
    //pipe
    int fwd_pipe[ENDS];
    int bkwd_pipe[ENDS];

    if ( pipe(fwd_pipe) == ERR || pipe(bkwd_pipe) == ERR)  {
        perror("pipes not created"), exit(ERR);
    }

    //pid types to hold processes
    pid_t child_first;
    pid_t child_second;
    pid_t wait_first;
    pid_t wait_second;

    child_first = fork();

    if (child_first == 0) { 
        //Everything in this if-block is part of the first child process only
        char msg[] = "Starting: ";
        strcat(msg, argv[1]);
        strcat(msg, "\n");
        write(STDERR, msg, strlen(msg)); 

        dup2(fwd_pipe[STDOUT], STDOUT);
        close(fwd_pipe[STDOUT]);
        dup2(bkwd_pipe[STDIN], STDIN);
        close(fwd_pipe[STDIN]);
        close(bkwd_pipe[STDIN]);
        close(bkwd_pipe[STDOUT]);
        
        //This is to pass no commands 
        char *cmds[2];
        cmds[0] = argv[1];
        cmds[1] = NULL;
        execvp(argv[1], cmds);

        exit(OK); 
    
    } else {
        //Currently back in parent process 
        child_second = fork(); 
        if (child_second == 0) {
            //Everything in this if-block is part of the second child process
            //printf("starting: %s\n", argv[2]);
            char msg[] = "Starting: ";
            strcat(msg, argv[2]);
            strcat(msg, "\n");
            write(STDERR, msg, strlen(msg)); 
            
            dup2(fwd_pipe[STDIN], STDIN);
            close(FD);
            //close(bkwd_pipe[STDIN]);
            dup2(bkwd_pipe[STDOUT], STDOUT);
            close(fwd_pipe[STDIN]);
            close(bkwd_pipe[STDOUT]);
            close(fwd_pipe[STDOUT]);
            
            //Start argv at 2
            execvp(argv[2], &argv[2]);

            exit(OK);
        }
        //Everything after here is part of the parent process again
        close(fwd_pipe[STDOUT]);
        close(bkwd_pipe[STDIN]);
        close(fwd_pipe[STDIN]);
        close(bkwd_pipe[STDOUT]);
        
        //Not entirely sure why the first param is -1, but 
        //am doing so to match strace of dpipe-4404
        //I would think it makes more sense to pass the pid
        //of the child processes as the first param.
        wait_second = wait4(-1, NULL, 0, NULL);
        wait_first = wait4(-1, NULL, 0, NULL);

        exit(OK);    
    }
    
    return OK;

}
